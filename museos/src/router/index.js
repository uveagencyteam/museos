import Vue from 'vue'
import Router from 'vue-router'
import ListMuseum from '@/views/ListMuseum'
import DetailMuseum from '@/views/DetailMuseum'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/list'
    },
    {
      path: '/list',
      name: 'ListMuseum',
      component: ListMuseum
    },
    {
      path: '/detail/:id',
      name: 'DetailMuseum',
      component: DetailMuseum
    }
  ]
})
